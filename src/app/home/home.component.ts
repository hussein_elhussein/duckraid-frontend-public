import {Component, EventEmitter, HostListener, Inject, OnInit, PLATFORM_ID, ViewEncapsulation} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {isPlatformBrowser, Location} from '@angular/common';
import {ActivatedRoute, NavigationEnd, Router,} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs/Observable';

import {ICategory} from '../interfaces/ICategory';
import {PostService} from '../services/post.service';
import {ScrollToConfigOptions, ScrollToService} from '@nicky-lenaers/ngx-scroll-to';
import {IPost} from '../interfaces/IPost';
import {SettingsService} from '../services/settings.service';
import {ISiteSettings} from '../interfaces/ISiteSettings';
import {IPostPag} from '../interfaces/IPostPag';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {PostDetailsComponent} from '../post-details/post-details.component';
import {ContactComponent} from '../contact/contact.component';
import {ISocialPages} from '../interfaces/ISocialPages';
import {IContactSettings} from '../interfaces/IContactSettings';
import {IPage} from '../interfaces/IPage';
import {ICRoute} from '../interfaces/ICRoute';
import {ResponsiveService} from '../services/responsive.service';
import {RouteHelperService} from '../services/route-helper.service';

//trace.enable();
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[Location],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fade', [
      state('in', style({opacity: '1'})),
      transition(':enter', [
        style({opacity: '0'}),
        animate(1000)
      ]),
      transition(':leave', [
        animate(500,style({opacity: '0'}))
      ])
    ]),
    trigger('moveRefine', [
      state('left', style({
        right: '300px'
      })),
      state('right',   style({
        right: '40px'
      })),
      //open:
      transition('right => left', animate('280ms ease-in')),
      //close:
      transition('left => right', animate('300ms ease-out'))
    ]),
    trigger('loading',[
      state('in', style({opacity: '1'})),
      transition(':leave', [
        animate(700, style({opacity:  '0'}))
      ])
    ]),
    trigger('overlay_fade', [
      state('in', style({opacity: '1'})),
      transition(':enter', [
        style({opacity: '0'}),
        animate(300)
      ]),
      transition(':leave', [
        animate(300, style({opacity:  '0'}))
      ])
    ]),
  ]
})
export class HomeComponent implements OnInit{
  public _opened: boolean;
  public current_side: string;
  public site_settings:ISiteSettings;
  public cats:ICategory[];
  public headers_posts: IPost[];
  public current_header: IPost;
  public headers_sorted: boolean;
  public gen_next_header: boolean;
  public storage:string;
  public posts:IPostPag;
  public pagination:IPostPag;
  public loading: boolean;
  public loading_progress: number;
  public loading_items: boolean;
  public current_device: string;
  public onResize:EventEmitter<any>;
  public social_pages:ISocialPages;
  public contact_details:IContactSettings;
  public about_page:IPage;
  public about_opened: boolean;
  public skip_nav: boolean;
  public current_route:ICRoute;
  @HostListener('window:resize', ['$event'])
  resize(event) {
    this.onResize.emit();
  }

  constructor
  (
    private settings:SettingsService,
    @Inject(PLATFORM_ID) private platformId: Object,
    private post_ser:PostService,
    private _scrollToService: ScrollToService,
    private dialog:MatDialog,
    private router:Router,
    private active_route:ActivatedRoute,
    private location:Location,
    private meta:Meta,
    private title:Title,
    private res_ser:ResponsiveService,
    private route_helper:RouteHelperService,
  ) {
    this._opened = false;
    this.current_side = 'right';
    this.about_opened = false;
    this.storage = settings.storage_base;
    this.cats = [];
    this.headers_posts = [];
    this.headers_sorted = false;
    this.gen_next_header = false;
    this.loading = true;
    this.loading_items = false;
    this.current_device = res_ser.getCurrentDevice();
    this.onResize = new EventEmitter<any>();
    this.loading_progress = 0;
    this.skip_nav = false;

  }

  ngOnInit() {
    this.checkState();
    this.route_helper.goToPost.subscribe(post => {
      this.goToPost(post);
    });
    this.router.events.forEach( ev=> {
      if(ev instanceof NavigationEnd) {
        if(!this.skip_nav){
          this.checkNav(ev.url);
        }
      }
    });
  }
  getLanding():string{
    return this.route_helper.determineLanding(this.cats);
  }
  //when the page first launched
  checkState():void{
    this.post_ser.catNavs().subscribe(navs => {
      this.cats = navs;
      let route = this.route_helper.getCurrentRoute(this.router.url,navs);
      this.current_route = route;
      if(!route){
        this.initData().subscribe((progress:any) => {
          this.loading_progress = this.calcPercentage(progress);
        },null,() => {
          this.loadLanding();
        });
      }else{
        if(route.name === 'home'){
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.calcPercentage(progress);
          },null,() => {
            this.addMeta();
          });
        }
        if(route.name === 'about'){
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.loading_progress = this.calcPercentage(progress);
          },null,() => {
            this.openAbout();
          });
        }
        if(route.name === 'contact'){
          const inquiry_type = route.params[1];
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.calcPercentage(progress);
          },null,() => {
            this.openContact(inquiry_type,false);
          });
        }
        if(route.name === 'category'){
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.calcPercentage(progress);
          },null,() => {
            const cat_title = route.params[0];
            const page = Number(route.params[1]);
            this.loadCat(cat_title,page);
          });
        }
        if(route.name === 'all-work'){
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.calcPercentage(progress);
          },null,() => {
          });
          const page = Number(route.params[1]);
          const url = 'post/all-work?page=' + page;
          this.loadNextPage(url,(res) => {
            if(!res){
              this.router.navigate(['']);
            }
          });
        }
        if(route.name === 'post'){
          this.initData().subscribe((progress:any) => {
            this.loading_progress = this.calcPercentage(progress);
          },null,() => {
            this.loadPost(route.params[0],route.params[1],route.params[2]);
          });
        }
      }
    });
  }
  openAbout($ev = null):void{
    if($ev){
      $ev.preventDefault();
    }
    this.current_route = this.route_helper.getCurrentRoute('/about',this.cats);
    this.location.go('/about');
    this.about_opened = true;
    if(this._opened){
      this.toggleMenu();
    }
    this.addMeta(
      this.site_settings.title + ' | ' + this.about_page.title,
      this.about_page.meta_keywords,
      this.about_page.meta_description
      );
  }
  goHome($ev = null){
    this.skip_nav = false;
    if($ev){
      $ev.preventDefault();
    }
    this.current_route = this.route_helper.getCurrentRoute('/',this.cats);
    this.location.go("");
    this.pagination = null;
    this.posts = null;
  }
  calcPercentage(progress:any):number{
    let res =  parseFloat(((progress.steps/progress.total) * 100).toFixed(1));
    if(res >= 100){
      if(isPlatformBrowser(this.platformId)){
        setTimeout(() => {
          this.loading = false;
        },500);
      }else{
        this.loading = false;
      }
    }
    return res;
  }
  goToPost(post_item:IPost):void{
    let cat = null;
    let url = this.location.path();
    let route = this.route_helper.getCurrentRoute(url,this.cats);
    //if we are in 'all-work' url, keep this path in the url and append the post slug:
    if(route.category === 'all-work'){
      cat = 'all-work';
    }else{
      //let target_cat = post_item.mini_category.name.replace(' ', '-').toLowerCase();
      let target_cat = post_item.mini_category.filter(item => {
        let name = item.name.replace(' ', '-').toLowerCase();
        if(name === route.category){
          return item;
        }
      });
      if(target_cat.length > 0){
        cat = target_cat[0].name.replace(' ', '-').toLowerCase();
      }else{
        cat = "all-work";
      }
    }
    this.about_opened = false;
    let path = cat + '/' + post_item.page;
    path += '/' + post_item.slug;
    this.skip_nav = true;
    this.router.navigate([path]);
    if(isPlatformBrowser(this.platformId)){
      setTimeout(()=>{
        this.skip_nav = false;
      },200);
    }else{
      this.skip_nav = false;
    }
    this.openPostPopup(post_item);
    this.addMeta(
      this.site_settings.title + ' | ' + post_item.title,
      post_item.meta_keywords,
      post_item.meta_description
    );
  }
  loadPost(cat_title:string,page_number:string,post_slug:string):void{
    let url = null;
    if(cat_title === 'all-work'){
      url = this.settings.base_url + "post/all-work?page=" + page_number;
    }else{
      url = this.settings.base_url + "post/category/" + cat_title + "?page=" + page_number;
    }
    this.post_ser.nextPage(url).subscribe((res) => {
      let res_data = res.data;
      res.data = [];
      this.posts = res;
      this.pagination = res;
      this.posts.data = this.assignPageNumbers(res_data);
      let founded_post = <IPost> this.getPost(post_slug);
      if(founded_post){
        this.addMeta(
          this.site_settings.title + ' | ' + founded_post.title,
          founded_post.meta_keywords,
          founded_post.meta_description
          );
        //timeout only on client side
        if(isPlatformBrowser(this.platformId)){
          const config: ScrollToConfigOptions = {
            target: 'post_item_' + founded_post.id,
            duration: 1500,
          };
          setTimeout(() => {
            this._scrollToService.scrollTo(config).subscribe(scroll_res => {});
          },200);
          setTimeout(()=>{
            this.openPostPopup(founded_post);
          },1700);
        }else{
          this.openPostPopup(founded_post);
        }
      }
    },err => {
      this.loadLanding();
    });

  }
  openPostPopup(post_item: IPost){
    this._opened = false;
    let device = this.current_device;
    let dialog = <MatDialogRef<any>> null;
    let last_device = null;
    let onDeviceChange = new EventEmitter<any>();
    let img_w = null;
    let img_h = null;
    let dimension = this.calcPostPopupHeight();
    let resLoaded = (width,height) => {
      if(dialog){
        img_w = width;
        img_h=  height;
        if(post_item.video){
          if(post_item.video && device === 'phone'){
            img_w = width + 100;
            width += 100;
          }else{
            img_w = width + 10;
            width += 10;
          }
        }
        let calc_res = this.calcPostPopupHeight(width,height);
        dialog.updateSize(calc_res.width,calc_res.height);
      }
    };
    dialog = this.dialog.open(PostDetailsComponent, {
      height: dimension.height,
      width: dimension.width,
      data: { post: post_item,onResLoaded:resLoaded,onDeviceChange:onDeviceChange},
    });
    if(!post_item.video) {
      let resize_sub = this.onResize.subscribe(() => {
        let cal_res = null;
        device = this.res_ser.getCurrentDevice();
        if (device !== last_device) {
          onDeviceChange.emit();
          cal_res = this.calcPostPopupHeight();
          dialog = dialog.updateSize(cal_res.width + 10, cal_res.height);
          last_device = device;
        }
      });
      dialog.afterClosed().subscribe(() => {
        resize_sub.unsubscribe();
      });
    }
  }
  calcPostPopupHeight(img_w: number = null, img_h: number = null):any{
    let height = null;
    let width = null;
    let device = this.current_device;
    switch (device){
      case "large_desktop":
        if(img_h){
          height = img_h + 50;
        }else{
          height = "99%";
        }
        if(img_w){
          width = img_w;
        }else{
          width = "98%";
        }
        break;
      case "desktop":
        if(img_h){
          height = img_h + 50;
        }else{
          height = '83vh';
        }
        if(img_w){
          width = img_w;
        }else{
          width = "80%";
        }
        break;
      case "tablet":
        if(img_h){
          height = img_h + 50;
        }else{
          height = "40vh";
        }
        if(img_w){
          width = img_w;
        }else{
          width = "80%";
        }
        break;
      case "phone":
        if(img_h){
          height = img_h + 50;
        }else{
          height = "33vh";
        }
        if(img_w){
          width = img_w;
        }else{
          width = "99%";
        }
        break;
    }
    if(img_w || img_h){
      return {height: height.toString() + 'px',width:width.toString() + 'px'};
    }else{
      return {height: height,width:width};
    }

  }
  getPost(slug:string):IPost{
    let post = <IPost>{};
    this.posts.data.forEach(item => {
      if(item.slug === slug){
        post = item;
      }
    });
    return post;
  }
  openContact(inquiry_type:string, navigate:boolean = true){
    let height = null;
    let width = null;
    let last_device = null;
    let device = null;
    let opened_dialog = <MatDialogRef<any>> null;
    let that = this;
    this.addMeta(
      this.site_settings.title + ' | ' + 'Contact Us',
      this.contact_details.keywords,
      this.contact_details.description
    );
    if(this._opened){
      this.toggleMenu();
    }
    let calc_size = function(){
      device = that.current_device;
      if(device === 'desktop' || device === 'large_desktop'){
        height = '90vh';
        width = '30%';
      }else{
        height = '82vh';
        width = '77%';
      }
    };
    calc_size();
    opened_dialog = this.dialog.open(ContactComponent, {
      height: height,
      width: width,
      disableClose: true,
      data: {contact_details:this.contact_details,inquiry_type:inquiry_type},
    });
    let resize_sub = this.onResize.subscribe(() => {
      calc_size();
      if(device !== last_device){
        opened_dialog = opened_dialog.updateSize(width,height);
        last_device = device;
      }
    });
    opened_dialog.afterClosed().subscribe(() => {
      resize_sub.unsubscribe();
    });
    if(navigate){
      this.skip_nav = true;
      this.router.navigate(['contact',inquiry_type]);
      if(isPlatformBrowser(this.platformId)){
        setTimeout(() => {
          this.skip_nav = false;
        },100);
      }else{
        this.skip_nav = false;
      }
    }
  }
  initData():Observable<any>{
    return new Observable<any>(observer => {
      //get the site settings
      let progress = {
        steps:0,
        total: 5
      };
      let checkAllLoaded = ()=>{
        if(progress.steps === 5){
          observer.complete();
        }
      };
      this.settings.getSettings().subscribe(settings_res => {
        this.site_settings = settings_res;
        progress.steps += 1;
        observer.next(progress);
        checkAllLoaded();
      },err => {
        console.log(err);
      });
      this.settings.getAboutPage().subscribe(page => {
        this.about_page = page;
        progress.steps += 1;
        observer.next(progress);
        checkAllLoaded();
      },err => {
        console.log(err);
      });
      this.settings.getSocialPages().subscribe(social => {
        this.social_pages = social;
        progress.steps += 1;
        observer.next(progress);
        checkAllLoaded();
      },err => {
        console.log(err);
      });
      this.settings.getContactDetails().subscribe(contact_detials => {
        this.contact_details = contact_detials;
        progress.steps += 1;
        observer.next(progress);
        checkAllLoaded();
      },err => {
        console.log(err);
      });

      //get site header:
      this.post_ser.headers().subscribe(headers => {
        this.headers_posts = headers;
        progress.steps += 1;
        console.log('got headers');
        this.current_header = this.randomHeader(headers);
        observer.next(progress);
        checkAllLoaded();
      }, err => {
        console.log(err);
      });
    });
  }
  loadLanding():void{
    let landing = this.route_helper.determineLanding(this.cats).replace(' ','-');
    this.router.navigate([landing,'1']);
  }
  toggleMenu():void{
    let res = this.res_ser.getScreenRes();
    if(this._opened){
      //close
      this._opened = false;
      //animate on tablet and above:
      if(res.width >= 768){
        if(isPlatformBrowser(this.platformId)){
          setTimeout(() => {
            this.current_side = 'right';
          },50);
        }else{
          this.current_side = 'right';
        }
      }
    }else{
      //open
      if(res.width >= 768){
        this.current_side = 'left';
      }
      if(isPlatformBrowser(this.platformId)){
        setTimeout(() => {
          this._opened = true;
        },100);
      }else{
        this._opened = true;
      }
    }
  }
  loadCat(name: string, page:number = 1){
    this.posts = null;
    this.loading_items = true;
    let mod_name = name.replace('-',' ');
    let cat_obj = this.cats.filter(item => {
      return item.name === mod_name;
    })[0];
    if(cat_obj){
      this.addMeta(this.site_settings.title + ' | ' + cat_obj.name,cat_obj.keywords);
    }
    if(page){
      mod_name += '?page=' + page;
    }
    this.post_ser.category(mod_name).subscribe(res => {
      let res_data = res.data;
      res.data = [];
      this.posts = res;
      this.pagination = res;
      this.posts.data = this.assignPageNumbers(res_data);
      this.loading_items = false;
      //timout only on client side
      if(isPlatformBrowser(this.platformId)){
        const config: ScrollToConfigOptions = {
          target: 'content_start',
          duration: 1000,
        };
        setTimeout(()=>{
          this._scrollToService.scrollTo(config);
        },200);
      }
    },err => {
      console.log(err);
      this.router.navigate(['']);
    });
  }
  loadNextPage(url:string = null, callback = null, navigate:boolean = true,scroll:boolean = true):void{
    let doScroll = () => {
      //timout only on client side
      if(isPlatformBrowser(this.platformId)){
        const config: ScrollToConfigOptions = {
          target: 'content_start',
          duration: 1000,
        };
        setTimeout(()=>{
          this._scrollToService.scrollTo(config);
        },200);
      }
    };
    if(!this.loading_items){
      let _url = null;
      if(url){
        _url = this.settings.base_url + url;
      }else{
        //check if current page is home page:
        if(this.current_route.name === 'home' && !this.pagination){
          let path = this.current_route.params[0];
          let page = this.current_route.params[1];
          _url = this.settings.base_url + 'post/' + path + '?page=' + page;
        }else{
          if(this.pagination){
            _url = this.pagination.next_page_url;
          }else{
            if(!this.about_opened){
              this.loadLanding();
            }
          }
        }
      }
      //if there still pages, load next:
      if(_url){
        this.loading_items = true;
        console.log(_url);
        this.post_ser.nextPage(_url).subscribe(res => {
          let res_data = res.data;
          res.data = [];
          this.pagination = res;
          res_data = this.assignPageNumbers(res_data);
          if(!this.posts){
            this.posts = res;
            this.posts.data = res_data;
          }else{
            res_data.forEach(post => {
              this.posts.data.push(post);
            });
          }
          this.loading_items = false;
          //scroll if not called from dom
          if(url && scroll){
            doScroll();
          }
          if(navigate){
            let page_number = this.pagination.current_page.toString();
            let path = [];
            path[0] = this.current_route.params[0];
            path[1] = page_number;
            this.current_route.params[1] = page_number;
            this.skip_nav = true;
            this.router.navigate(path);
            if(isPlatformBrowser(this.platformId)){
              setTimeout(()=>{
                this.skip_nav = false;
              },300);
            }else{
              this.skip_nav = false;
            }
          }
          if(res.data.length){
            if(callback){
              callback(true);
            }
          }else{
            if(callback){
              callback(false);
            }
          }
        },err => {
          if(callback){
            callback(false);
          }
        });
      }
    }
  }
  addMeta(title:string = null,keywords:string = null,description:string = null):void{
    let _t = this.site_settings.title + ' | ' + this.site_settings.description;
    let _k = this.site_settings.keywords;
    let _d = this.site_settings.description;
    if(title && title.length) {
      _t  = title;
    }
    this.title.setTitle(_t);
    if(isPlatformBrowser(this.platformId)){
      if(keywords && keywords.length){
        _k = keywords;
      }
      if(description && description.length){
        _d = description;
      }
      this.meta.updateTag({ name: 'keywords', content: _k});
      this.meta.updateTag({ name: 'description', content: _d });
    }
  }
  checkNav(url:string):void{
    let route  = this.route_helper.getCurrentRoute(url,this.cats);
    if(route && !this.route_helper.isLinkActive(route.prev)){
      this.current_route = route;
      if(this._opened){
        this.toggleMenu();
      }
      this.about_opened = false;
      switch (route.name){
        case 'home':
          this.posts = null;
          this.pagination = null;
          this.loadNextPage();
          break;
        case 'about':
          this.openAbout();
          break;
        case 'contact':
          this.openContact(route.params[1]);
          break;
        case 'category':
          this.posts = null;
          this.pagination = null;
          this.loadCat(route.params[0]);
          break;
        case 'all-work':
          this.posts = null;
          this.pagination = null;
          const _url = 'post/all-work?page=1';
          this.loadNextPage(_url,null,true,true);
          break;
        case 'post':
          const post = this.getPost(route.params[2]);
          this.openPostPopup(post);

      }
    }else{
      if(this._opened){
        this.toggleMenu();
      }
      console.log('cannot reactivate same route','current route: ', this.current_route.path,'prev route: ' ,this.current_route.prev);
    }
  }

  randomHeader(headers:IPost[]):IPost{
    let index = Math.floor(Math.random() * Math.floor(headers.length -1));
    return headers[index];
  }
  nextHeader():void{
    if(!this.gen_next_header){
      let title = this.current_header.title;
      this.current_header = null;
      this.gen_next_header = true;
      let generated_next = null;
      if(!this.headers_sorted){
        this.headers_posts = this.headers_posts.sort((a,b) => {
          if(a.title === title){
            return 1;
          }else{
            return 0;
          }
        });
        generated_next = this.headers_posts[0];
        this.headers_sorted = true;
      }else{
        //find next header index:
        let next_index = null;
        for(let i = 0;i < this.headers_posts.length;i++){
          if(this.headers_posts[i].title === title){
            next_index = i + 1;
          }
        }
        if(next_index === this.headers_posts.length){
          next_index = 0;
        }
        generated_next = this.headers_posts[next_index];
      }

      if(isPlatformBrowser(this.platformId)){
        setTimeout(() => {
          this.current_header = generated_next;
          this.gen_next_header = false;
        },400);
      }else{
        this.current_header = generated_next;
        this.gen_next_header = false;
      }
    }
  }
  assignPageNumbers(posts:IPost[]):IPost[]{
    posts.forEach(post => {
      post.page = this.pagination.current_page;
    });
    return posts;
  }
  getImage(post:IPost):string{
    let ports = post.image.split('.');
    let ext = ports[ports.length -1];
    return this.storage + post.image.replace('.' + ext,'-original' + '.' + ext);
  }
  getColsProp():string{
    if(this.current_route.category === 'all-work'){
      return 'in_all_columns';
    }else{
      return 'columns';
    }
  }
  getRowsProp():string{
    if(this.current_route.category === 'all-work'){
      return 'in_all_rows';
    }else{
      return 'rows';
    }
  }
}
